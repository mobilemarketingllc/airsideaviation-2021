var $ = jQuery;
$(document).ready(function(){
    if($('.featured-products > .featured-product-list > .featured-product-item').length > 3){
        $('.featured-products > .featured-product-list').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 4,
            slidesToScroll: 4,
            responsive: [
                {
                    breakpoint: 899,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
});
/*$(window).ready(function(){
	$("body").on("click",".facetwp-page",function(){
		$("html, body").animate({ scrollTop: "0px" });
	});
}); */