<?php
//session_start();
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );
// Classes
require_once 'classes/class-fl-child-theme.php';
// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );
add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js",array("jquery"));
});
// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}
add_filter('wpseo_metabox_prio', 'yoasttobottom');

/* To hide counts from all dropdowns - FACETWP: */
add_filter( 'facetwp_facet_dropdown_show_counts', '__return_false' );

// Changing excerpt more
function new_excerpt_more($more) {
    return '';
}
add_filter('excerpt_more', 'new_excerpt_more', 21 );

function the_excerpt_more_link( $excerpt ){
    $post = get_post();
    $excerpt .= '<a href="'. get_permalink($post->ID) . '">Read More</a>';
    return $excerpt;
}
add_filter( 'the_excerpt', 'the_excerpt_more_link', 21 );

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'newspage-thumb' );
    set_post_thumbnail_size( 334, 222 ); // default Post Thumbnail dimensions   
}

if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'newspage-thumb',334, 222, true ); //(cropped)
}

/**
 * Filter the except length to 20 words.
 *
 * @param int $length Excerpt length.
 * @return int (Maybe) modified excerpt length.
 */
function wpdocs_custom_excerpt_length( $length ) {
    return 40;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

add_filter( 'the_excerpt', 'the_excerpt_more_link', 21 );

if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'newspagedetail-thumb' );
    set_post_thumbnail_size( 705, 401 ); // default Post Thumbnail dimensions   
}

if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'newspagedetail-thumb',705, 401, true ); //(cropped)
}
function wpb_custom_new_menu() {
  register_nav_menu('mobile-menu',__( 'Mobile Menu' ));
}
add_action( 'init', 'wpb_custom_new_menu' );
function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

add_filter( 'gform_field_value_refurl', 'populate_referral_url');
 
function populate_referral_url( $form ){
    // Grab URL from HTTP Server Var and put it into a variable
    $refurl = $_SERVER['HTTP_REFERER'];
 
    // Return that value to the form
    return esc_url_raw($refurl);
}

function featured_products_fun() {
	$args = array(
        'post_type' => array('jet', 'helicopter', 'turboprop', 'generalaviation'),
        'posts_per_page' => 8,
        'meta_query' => array(
            array(
                'key' => 'featured', 
                'value' => true, 
                'compare' => '=='
            )
        ) 
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ){
        $outpout = '<div class="featured-products"><div class="featured-product-list">';
        while ($query->have_posts()) : $query->the_post(); 
      $id=get_the_ID();
            $room_image = get_the_post_thumbnail_url(get_the_ID(),'full');
            
            if( get_field('enter_display_text') != '' ){
                $display_text_val = get_field('enter_display_text');
            }
            else {
                $display_text_val ='Reduced Price!';
            }

            if( get_field('sold') ) {
                $sold_text_val = '<span class="soldtext">Sold!</span>';
            }

            if( get_field('display_reduced_price_text', get_the_ID()) == 'show' ){
                $reduced_price_val = '<span class="reducedtext">'.$display_text_val.'</span>';
            }


            $outpout .= '<div class="featured-product-item">
                <div class="fl-post-grid-post airlist">
                    <div class="prod-img-wrap">
                        <div class="inventory-img">
                            '.$reduced_price_val.'
                            '.$sold_text_val.'
                            <img src="'.$room_image.'" alt="'.get_the_title().'" />
                        </div>
                        <div class="fl-post-grid-text product-grid">
                            <div class="product-grid-top">
                                <h4 style="text-align: center;">'.get_the_title().'</h4>';	
                    
                                if( get_field('office_price') ): 
                                    $outpout .= '<p class="office_price align_center">Office: +/- '.get_field('office_price').' SF</p>';
                                endif;

                                if( get_field('warehouse_price') ):
                                    $outpout .= '<p class="warehouse_price align_center">Warehouse: +/- '.get_field('warehouse_price').' SF</p>';
                                endif;

                                if( get_field('office_price') || get_field('warehouse_price') ):
                                    $total = intval(str_replace(',', '', get_field('warehouse_price'))) + intval(str_replace(',', '', get_field('office_price'))); 
                                    $outpout .= '<p class="office_price align_center">Total: +/- '.$total.' SF</p>';
                                endif;

                                if( get_field('office_price') || get_field('warehouse_price') ){} else {
                                    if( get_field('year_built') ):
                                        $outpout .='<p class="plane-year">Year <strong>'.get_field('year_built').'</strong></p>';
                                    endif;
                                    if( get_field('price') ):
                                        $outpout .='<p class="customPrice" style="text-align: center; color: #f89621;"><strong>$'. get_field('price').'</strong></p>';
                                    endif;
                                }
                                
                                if( get_field('custom_price') ):
                                    $outpout .= '<div class="custom_price_wrap">';
                                endif;
                                $outpout .= '<div class="home-divider"></div>';
                                
                                if( get_field('custom_price') ):
                                    $outpout .= '<p style="text-align: center; color: #f89621;"><strong>'.get_field('custom_price').'</strong></p>';
                                endif;
                                
                                if( get_field('custom_price') ):
                                    $outpout .= '</div>';
                                endif;

                                if( get_field('office_price') || get_field('warehouse_price') ){} else {
                                $outpout .= '<div class="icon-sep">';
                                    if( get_field('max_renge') ):
                                        $outpout .= '<div class="hours-icon"><img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/range.png" alt="" ><br>'.get_field('max_renge').'<br> Max Range</div>';
                                    endif;
                                    
                                    if( get_field('hours') ):
                                        $outpout .= '<div class="hours-icon"><img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/08/hours.png" alt="" ><br>'.get_field('hours').'<br>Hours</div>';
                                    endif;

                                    if( get_field('passengers') ):
                                        if( get_field('passengers')!=1 ){ $passengerslab =' Passengers'; } else{ $passengerslab =' Passenger';}
                                        $outpout .= '<div class="hours-icon"><img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/owners.png" alt="" ><br>'.get_field('passengers').' '.$passengerslab.'</div>';
                                    endif;
                                $outpout .= '</div>';
                                }

                                $outpout .= '
                            </div>
                        </div>
                    </div>
                </div>
            </div>';
            
        endwhile;
        $outpout .= '</div></div>';
        wp_reset_query();
    }  

    return $outpout;
}
add_shortcode( 'featured_products', 'featured_products_fun' );

function my_is_numeric($value)
  {
    return (preg_match ("/^(-){0,1}([0-9]+)(,[0-9][0-9][0-9])*([.][0-9]){0,1}([0-9]*)$/", $value) == 1);
  }

  function bbtheme_yoast_breadcrumbs() {

    if ( function_exists('yoast_breadcrumb') && ! is_front_page() ) {
      yoast_breadcrumb('<div class="container"><p id="breadcrumbs">','</p></div>');
    }
  
  }
  add_action( 'fl_content_open', 'bbtheme_yoast_breadcrumbs' );

  
 //Yoast SEO Breadcrumb link - Changes for PDP pages
add_filter( 'wpseo_breadcrumb_links', 'wpse_override_yoast_breadcrumb_trail',90 );

function wpse_override_yoast_breadcrumb_trail( $links ) {

    if (is_singular( 'generalaviation' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane',
            'text' => 'Find a Plane',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane/general-aviation/',
            'text' => 'General Aviation',
        );       
        array_splice( $links, 1, -1, $breadcrumb );
        
    } else if (is_singular( 'turboprop' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane',
            'text' => 'Find a Plane',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane/turbo-props/',
            'text' => 'Turbo Props',
        );       
        array_splice( $links, 1, -1, $breadcrumb );
        
    }else if (is_singular( 'jet' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane',
            'text' => 'Find a Plane',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane/jets/',
            'text' => 'Jets',
        );       
        array_splice( $links, 1, -1, $breadcrumb );
        
    }else if (is_singular( 'helicopter' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane',
            'text' => 'Find a Plane',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane/helicopters/',
            'text' => 'Helicopters',
        );       
        array_splice( $links, 1, -1, $breadcrumb );
        
    }else if (is_singular( 'hangar' )) {
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane',
            'text' => 'Find a Plane',
        );
        $breadcrumb[] = array(
            'url' => get_site_url().'/find-a-plane/hangar/',
            'text' => 'Hangar',
        );       
        array_splice( $links, 1, -1, $breadcrumb );
        
    }

    
    
    return $links;
}