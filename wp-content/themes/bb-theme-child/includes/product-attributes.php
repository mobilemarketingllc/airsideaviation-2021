<div class="product-attributes">
    <h3>Product Attributes</h3>
    <table class="table table-striped">
        <thead>
        <tbody>
        <?php if(get_field('color_range')) { ?>
         <tr>
            <th scope="row">Color Range</th>
            <td><?php the_field('color_range'); ?></td>
        </tr>
        <?php } ?>
        <?php if(get_field('color_tones')) { ?>
            <tr>
                <th scope="row">Color Tones</th>
                <td><?php the_field('color_tones'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('species')) { ?>
        <tr>
            <th scope="row">Species</th>
            <td><?php the_field('species'); ?></td>
        </tr>
        <?php } ?>
        <?php if(get_field('carpet_category')) { ?>
            <tr>
                <th scope="row">Category</th>
                <td><?php the_field('carpet_category'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('hardwood_category')) { ?>
        <tr>
            <th scope="row">Category</th>
            <td><?php the_field('hardwood_category'); ?></td>
        </tr>
        <?php } ?>
        <?php if(get_field('laminate_category')) { ?>
            <tr>
                <th scope="row">Category</th>
                <td><?php the_field('laminate_category'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('fiber_type')) { ?>
            <tr>
                <th scope="row">Fiber Type</th>
                <td><?php the_field('fiber_type'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('surface_type')) { ?>
            <tr>
                <th scope="row">Surface Type</th>
                <td><?php the_field('surface_type'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('style')) { ?>
            <tr>
                <th scope="row">Style</th>
                <td><?php the_field('style'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('edge_profile')) { ?>
            <tr>
                <th scope="row">Edge Profile</th>
                <td><?php the_field('edge_profile'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('application')) { ?>
            <tr>
                <th scope="row">Application</th>
                <td><?php the_field('application'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('Size')) { ?>
            <tr>
                <th scope="row">Size</th>
                <td><?php the_field('size'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('width')) { ?>
            <tr>
                <th scope="row">Width</th>
                <td><?php the_field('width'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('length')) { ?>
            <tr>
                <th scope="row">Length</th>
                <td><?php the_field('length'); ?></td>
            </tr>
        <?php } ?>
        <?php if(get_field('thickness')) { ?>
            <tr>
                <th scope="row">Thickness</th>
                <td><?php the_field('thickness'); ?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
    </div>