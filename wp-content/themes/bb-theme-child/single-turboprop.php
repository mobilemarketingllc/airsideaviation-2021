<?php get_header(); ?>
<!--
<div class="container">
    <div class="row">
    <div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
       <a href="<?php //echo site_url(); ?>/index.php/carpeting-catalog/">Carpeting Catalog</a> &raquo;
        <?php //the_title(); ?>
    </div>
        </div>
</div>
-->

 

<div class="container product-detail">
	<div class="row">
		<div class="fl-content product">
			<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
			<?php
//$show_thumbs = FLTheme::get_setting('fl-posts-show-thumbs');
?>
<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?php  echo get_stylesheet_directory_uri(); ?>/css/slick-theme.css">

<script src="<?php  echo get_stylesheet_directory_uri(); ?>/js/slick.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php  echo get_stylesheet_directory_uri(); ?>/js/accordion.js" type="text/javascript" charset="utf-8"></script>

<script type="text/javascript">
jQuery(document).ready(function(){
       
  jQuery('.jetslider').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: '.slider-nav'
  });
jQuery('.slider-nav').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.jetslider',
  dots: true,
  centerMode: false,
  focusOnSelect: true
});
    });
  </script>

<script type="text/javascript">
jQuery(document).ready(function(){
getAccordion("#tabs",768);
});
</script>

<article <?php post_class( 'fl-post' ); ?> id="fl-post-<?php the_ID(); ?>" >

            <?php
            $title = get_the_title();
            $images = get_field('turbo_prop_gallery');

            foreach( $images as $image ){
               
               $imagetop = wp_get_attachment_image_url( $image['ID'], $size ); 
               break;
            }

			$jsonld = array('@context'=>'https://schema.org/','@type'=>'Product','name'=> $title,'image'=>$imagetop,'description'=>$title,'sku'=>get_the_ID(),'mpn'=>get_the_ID(),'brand'=>array('@type'=>'thing','name'=>'turboprop'), 
			'offers'=>array('@type'=>'offer','priceCurrency'=>'USD','price'=>'00','priceValidUntil'=>''));
			?>
			<?php echo '<script type="application/ld+json">'.json_encode($jsonld).'</script>';	?>

	<header class="fl-post-header">

		<?php //FLTheme::post_top_meta(); ?>
	</header><!-- .fl-post-header -->

	<div class="fl-post-content clearfix" itemprop="text">


        <div class="row">
            <div class="col-md-6 col-sm-6 product-box">
				
				
			<?php 					 

				if(isMobile()){?>
					<h2 class="productTitle" itemprop="name"><?php echo get_the_title(); ?></h2>
				<?php
				}
				else {
					
				}							 
			?>	
				
            <?php if ( has_post_thumbnail() ) { ?>
             <!--   <img width="720" height="480" src="<?php echo get_the_post_thumbnail_url(); ?>" 
        class="attachment-large size-large wp-post-image" alt="" itemprop="image" sizes="(max-width: 720px) 100vw, 720px">-->
             <?php } ?>
              
             
             <div class="jetslider">
             <?php

$images = get_field('turbo_prop_gallery');
$size = 'full'; // (thumbnail, medium, large, full or custom size)  

if( $images ): ?>
       
        <?php foreach( $images as $image ): ?>
            <div>
            	<?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
            </div>
        <?php endforeach; ?>
             
 <?php endif; ?>       

         <?php if( get_field('youtube_video_id') ): ?>  
                    <div>                      
                        <iframe type="text/html" width="440" height="305" src="https://www.youtube.com/embed/<?php the_field('youtube_video_id'); ?>?rel=0" frameborder="0"></iframe>
                    </div>
         <?php endif; ?> 

            </div>
            <div class="slider-nav">
            <?php if( $images ): 
                    foreach( $images as $image ): ?>
                        <div>
            	            <?php echo wp_get_attachment_image( $image['ID'],'thumbnail' ); ?>
                        </div>
                <?php endforeach; endif; ?> 
                <?php if( get_field('youtube_video_id') ): ?>  
                    <div>                      
                        <img src="https://img.youtube.com/vi/<?php the_field('youtube_video_id'); ?>/maxresdefault.jpg"/>
                    </div>
                <?php endif; ?>          
            </div>
          
 </div>
         <div class="col-md-4 col-sm-4 product-atts">
			 
			<?php 					 

				if(isMobile()){
					
				}
				else {
					?>
					<h2 class="productTitle" itemprop="name"><?php echo get_the_title(); ?></h2>
			 <?php
				}							 
			?>	
                 
                    
                    <?php //if( get_field('make') ): ?>
					<!-- <p class="plane-make"><strong><?php  the_field('make'); ?></strong></p> -->
                    <?php //endif; ?>
                    <?php if( get_field('airframe_serial') ): ?>
                    <p class="plane-make"><strong>SN:<?php  the_field('airframe_serial'); ?></strong></p>
                   <?php endif; ?>
                     <?php if( get_field('display_reduced_price_text') == 'show' ): ?>
			 <p class="reduced-price"> <label><?php if( get_field('enter_display_text') != '' ){echo the_field('enter_display_text');} else { echo 'Reduced Price!';} ?></label></p>
                    <?php endif; ?>
                    <?php if( get_field('price') ): ?>
                    <h3 class="aviation-price <?php if( get_field('sold') ){ echo 'soldOut';} ?>"><?php if (my_is_numeric(get_field('price'))) { echo '$';} ?><?php  the_field('price'); ?> <?php if( get_field('sold') ){ echo '<span>Sold</span>';} ?></h3>
                    <?php endif; ?>  
                    <?php if( get_field('acquired_aircrafts') ): ?>
                    <h3 class="aviation-price soldOut"><?php if( get_field('acquired_aircrafts') ){ echo 'Acquired Aircrafts';} ?></h3>
                    <?php endif; ?>    
			<div class="prductDetail">
                   <?php if( get_field('year_built') ): ?><p> <label>Year Built :</label><span><?php  the_field('year_built'); ?></span></p><?php endif; ?>
                   <?php if( get_field('registration') ): ?><p> <label>Registration :</label><span><?php  the_field('registration'); ?></span></p><?php endif; ?>
                   <?php //if( get_field('airframe_serial') ): ?> <!-- <p> <label>Airframe Serial :</label><span><?php  the_field('airframe_serial'); ?></span></p>--><?php //endif; ?> 
                   <?php if( get_field('airframe_total_time') ): ?><p><label>Airframe Total Time :</label><span><?php  the_field('airframe_total_time'); ?></span></p><?php endif; ?>
                   <?php if( get_field('tso_hrs') ): ?><p><label>TSO (hrs) :</label><span><?php  the_field('tso_hrs'); ?></span></p><?php endif; ?>
                   <?php if( get_field('airframe_cycles') ): ?><p><label>Airframe Cycles :</label><span><?php  the_field('airframe_cycles'); ?></span></p><?php endif; ?>
			</div>
                   <?php if(get_field('show_download_specs')=='true'){?>
                  <div class="download-button ">   
                    
			            <a href="<?php the_field('download_specification_document') ;?>" target="_self" class="dwbutton" role="button" download>Download Specs</a>
                   
                </div>
                   <?php } ?>
                   <?php if( get_field('price')=="" ){ ?>
                <div class="inquire-button">   
                   
			            <a href="/contact-us" target="_self" class="eqbutton" role="button">Inquire for Price</a>
                    
                </div>
                <?php } else{ ?>
                    <div class="inquire-button">   
                                    
                    <a href="/contact-us" target="_self" class="eqbutton" role="button">Inquire</a>

                    </div>  

              <?php  } ?>
              <div class="icon-sep jeticons" style="display:block;">
                      <?php if( get_field('max_renge') ): ?>
                        <div class="hours-icon">
                                    <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/range.png" alt="" ><br>
                                    <?php the_field('max_renge') ;?><br> Max Range</div>
                         <?php endif; ?>
                         <?php /*if( get_field('max_cruise_speed') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/max_speed.png" alt="" ><br>
                            <?php the_field('max_cruise_speed') ;?> <br>Max Cruise Speed</div>
                        <?php endif; */?>
				  <?php if( get_field('hours') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/08/hours.png" alt="" ><br>
                            <?php the_field('hours') ;?> <br>Hours</div>
                        <?php endif; ?>	
                        <?php if( get_field('passengers') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/owners.png" alt="" ><br>
                            <?php the_field('passengers') ;?> Passengers</div>
                        <?php endif; ?>
                 </div>


         </div>

                
                  
        </div>

          <div class="row infotabs">
            <div class="col-md-12 col-sm-12 product-infotabs">

                 <ul class="nav nav-tabs" id="tabs">
                 <?php if( get_field('engine_model')!='' || get_field('engine_serial')!=''|| get_field('engine_image')!='' ||  get_field('multiple_engines_and_props')): ?>
                <li class="active"><a data-toggle="tab" href="#engine">Engine</a></li>
                <?php endif; ?>
                <?php if( get_field('turbo_prop_avionics_features')!='' || get_field('turbo_prop_avionics_image')!='' ): ?><li><a data-toggle="tab" href="#avionics">Avionics</a></li><?php endif; ?>
                <?php if( get_field('turbo_prop_interior_info')!='' || get_field('turbo_prop_interior_image')!='' ): ?><li><a data-toggle="tab" href="#interior">Interior</a></li><?php endif; ?>
                <?php if( get_field('turbo_prop_exterior_info')!='' || get_field('turbo_prop_exterior_image')!='' ): ?><li><a data-toggle="tab" href="#exterior">Exterior</a></li><?php endif; ?>
                <?php if( get_field('turbo_prop_propeller_info')!='' || get_field('turbo_prop_option_info')!='' ): ?><li><a data-toggle="tab" href="#propeller">Propeller</a></li><?php endif; ?>
                <?php if( get_field('turbo_prop_option_info')!=''): ?><li><a data-toggle="tab" href="#options">Options</a></li><?php endif; ?>
                </ul>   

                <div class="tab-content">
                    <div id="engine" class="tab-pane fade in active">
                        <div class="row">
                        <div class="col-md-6 col-sm-6">
                        <?php if( get_field('engine_model') ): ?> <p> <label>Engine Model :</label><span><?php  the_field('engine_model'); ?></span></p><?php endif; ?>
						<?php if( get_field('multiple_engines_and_props') ): ?> <p> <label>Multiple Engines and Props :</label></p>
						<?php	// check if the flexible content field has rows of data
							if( have_rows('multiple_engines_and_props_values') ):
								 // loop through the rows of data
								while ( have_rows('multiple_engines_and_props_values') ) : the_row();								
									if( get_row_layout() == 'engine_number' ):
										?>
										 <p> <label><?php echo get_sub_field('label_engine'); ?> :</label><span><?php echo get_sub_field('engine_no'); ?></span></p>								
									<?php
									elseif( get_row_layout() == 'prop' ): 
									?>
										 <p> <label><?php echo get_sub_field('lable_prop'); ?> :</label><span><?php echo get_sub_field('prop_dec'); ?></span></p>								
									<?php
									endif;
								endwhile;
							else :
								// no layouts found
							endif;	
						?>
						<?php endif; ?>	
                        <?php if( get_field('engine_serial') ): ?> <p> <label>Engine Serial :</label><span><?php  the_field('engine_serial'); ?></span></p><?php endif; ?>
                            <?php if( get_field('engine_time') ): ?> <p> <label>Engine Time :</label><span><?php  the_field('engine_time'); ?></span></p><?php endif; ?>	
                       
                            
                         </div>   

                         <div class="col-md-6 col-sm-6">
                         <?php if( get_field('engine_image') ): ?>
                         <img width="720" height="480" src="<?php  the_field('engine_image'); ?>" 
        class="attachment-large size-large wp-post-image" alt="" itemprop="image" sizes="(max-width: 720px) 100vw, 720px">
                             <?php endif; ?>
                        </div>  

                        </div>
                    </div>
                    <div id="avionics" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                <?php if( get_field('turbo_prop_avionics_features') ): ?>
                                <?php the_field('turbo_prop_avionics_features'); ?>
                                <?php endif; ?>
                                </div> 
                                <div class="col-md-6 col-sm-6">
                                <?php if( get_field('turbo_prop_avionics_image') ): ?>
                                <img width="720" height="480" src="<?php  the_field('turbo_prop_avionics_image'); ?>" 
        class="attachment-large size-large wp-post-image" alt="" itemprop="image" sizes="(max-width: 720px) 100vw, 720px">
                                <?php endif; ?>
                                </div>   

                            </div>
                    </div>
                    <div id="interior" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-6 col-sm-6">
                                <?php if( get_field('turbo_prop_interior_info') ): ?>
                                <?php the_field('turbo_prop_interior_info'); ?>
                                <?php endif; ?>
                                </div> 
                                <div class="col-md-6 col-sm-6">
                                <?php if( get_field('turbo_prop_interior_image') ): ?>
                                <img width="720" height="480" src="<?php  the_field('turbo_prop_interior_image'); ?>" 
        class="attachment-large size-large wp-post-image" alt="" itemprop="image" sizes="(max-width: 720px) 100vw, 720px">
                                <?php endif; ?>
                                </div> 
                            </div>
                    </div>
                    <div id="exterior" class="tab-pane fade">
                            <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <?php if( get_field('turbo_prop_exterior_info') ): ?>
                                        <?php the_field('turbo_prop_exterior_info'); ?>
                                        <?php endif; ?>
                                        </div> 
                                        <div class="col-md-6 col-sm-6">
                                        <?php if( get_field('turbo_prop_exterior_image') ): ?>
                                        <img width="720" height="480" src="<?php  the_field('turbo_prop_exterior_image'); ?>" 
                class="attachment-large size-large wp-post-image" alt="" itemprop="image" sizes="(max-width: 720px) 100vw, 720px">
                                        <?php endif; ?>
                                        </div> 
                            </div>
                    </div>
                    <div id="propeller" class="tab-pane fade">
                          <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <?php if( get_field('turbo_prop_propeller_info') ): ?>
                                        <?php the_field('turbo_prop_propeller_info'); ?>
                                        <?php endif; ?>
                                        </div> 
                             </div> 
                    </div>

                    <div id="options" class="tab-pane fade">
                          <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                        <?php if( get_field('turbo_prop_option_info') ): ?>
                                        <?php the_field('turbo_prop_option_info'); ?>
                                        <?php endif; ?>
                                        </div> 
                             </div> 
                    </div>

                </div>
 <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true"></div>
<!-- /Bootstrap <a href="https://www.jqueryscript.net/accordion/">Accordion</a> -->
</div>
            </div>
         </div>


    </div><!-- .fl-post-content -->

</article>
<!-- .fl-post -->
			<?php endwhile; endif; ?>
		</div>
		<?php //FLTheme::sidebar('right'); ?>
	</div>
</div>
<?php get_footer(); ?>