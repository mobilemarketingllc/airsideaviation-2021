<div class="product-grid product-grids" itemscope="" itemtype="http://schema.org/ItemList">
    <div class="row"> 
    <?php
    $i= 1;
    if(have_posts()) :  ?>
<?php while ( have_posts() ): the_post();

if( get_field('sold')=='0' ){
?>
    <div class="col-md-3 col-sm-4 col-xs-12 product-grid-item" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">

    <meta itemprop="position" content="<?php echo $i; ?>">

    <div class="fl-post-grid-post airlist <?php if( get_field('custom_price') ): echo "hangar-list"; endif; ?>">
	<a href="<?php the_permalink(); ?>" title="<?php  the_title(); ?>" itemprop="url">
            <div class="inventory-img">
            
				
				<?php if( get_field('display_reduced_price_text') == 'show' && get_field('acquired_aircrafts') == 0 ) { ?>
                    <span class="reducedtext"><?php if( get_field('enter_display_text') != '' ){echo the_field('enter_display_text');} else { echo 'Reduced Price!';} ?></span>
                <?php } else if( get_field('sold') ){ ?>
                    <span class="soldtext">Sold!</span>
                <?php } else if( get_field('acquired_aircrafts') ){ ?>
                    <span class="soldtext">Acquired Aircraft!</span>
                <?php } ?>
               <img src="<?php the_post_thumbnail_url(); ?>" style="width:300px;height:200px;"/>
               
            </div>
        
		
        <div class="fl-post-grid-text product-grid">
 
            <div class="product-grid-top">
                    <h4 style="text-align: center;"><?php  the_title(); ?></h4>		
                    
                    
                    <?php if( get_field('office_price') ): ?>
                    <p class="office_price align_center">Office: +/- <?php  the_field('office_price'); ?> SF</p>
                   <?php endif; ?>

                   <?php if( get_field('warehouse_price') ): ?>
                    <p class="warehouse_price align_center">Warehouse: +/- <?php  the_field('warehouse_price'); ?> SF</p>
                   <?php endif; ?>

                   <?php if( get_field('office_price') || get_field('warehouse_price') ):
                    $total = intval(str_replace(',', '', get_field('warehouse_price'))) + intval(str_replace(',', '', get_field('office_price'))); 
                    ?>
                    <p class="office_price align_center">Total: +/- <?php   echo $total; ?> SF</p>
                   <?php endif; ?>

                    <?php if( get_field('office_price') || get_field('warehouse_price') ){} else { ?>
                    <p class="plane-year"><?php if( get_field('year_built') ): ?>Year <strong><?php the_field('year_built'); ?></strong><?php endif; ?></p>
					<p class="customPrice" style="text-align: center; color: #f89621;"><strong><?php if( get_field('price') ): ?><?php if (my_is_numeric(get_field('price'))) { echo '$';} ?><?php the_field('price'); ?><?php endif; ?></strong></p>	
                    <?php } ?>

                    <!-- <a href="<?php the_permalink(); ?>" class="view-cta fl-button">View Details</a> -->
                    
                    <?php if( get_field('custom_price') ): ?>
                        <div class="custom_price_wrap">
                    <?php endif; ?>
                        <div class="home-divider"></div>
                        <?php if( get_field('custom_price') ): ?>
                        <p style="text-align: center; color: #f89621;"><strong><?php the_field('custom_price'); ?></strong></p>	
                        <?php endif; ?>
                    <?php if( get_field('custom_price') ): ?>
                        </div>
                    <?php endif; ?>

                    <?php if( get_field('office_price') || get_field('warehouse_price') ){} else { ?>
					<div class="icon-sep">
                      <?php if( get_field('max_renge') ): ?>
                        <div class="hours-icon">
                                    <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/range.png" alt="" ><br>
                                    <?php the_field('max_renge') ;?><br> Max Range</div>
                         <?php endif; ?>
                         <?php /*if( get_field('max_cruise_speed') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/max_speed.png" alt="" ><br>
                            <?php the_field('max_cruise_speed') ;?> <br>Max Cruise Speed</div>
                        <?php endif;*/ ?>
						<?php if( get_field('hours') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/08/hours.png" alt="" ><br>
                            <?php the_field('hours') ;?> <br>Hours</div>
                        <?php endif; ?>		 
                        <?php if( get_field('passengers') ): ?>
                        <div class="hours-icon">
                            <img class="alignnone size-full wp-image-103" src="/wp-content/uploads/2018/05/owners.png" alt="" ><br>
                           <?php if( get_field('passengers')!=1 ){ echo get_field('passengers').' Passengers';}else{ echo get_field('passengers').' Passenger';}?> </div>
                        <?php endif; ?>
                    </div>
                    <?php } ?>

			</div>

		</div>
		</a>
	</div>
    </div>
<?php $i++;} endwhile; ?>
<?php else : ?>

<div class="noposts"><h4>We currently don't have any <?php echo get_the_title();?> in our inventory</h4></div>
<div class="fl-button-wrap fl-button-width-auto fl-button-center btnService nopostsbtn ">
			<a href="/find-a-plane/" target="_self" class="teambtn" role="button">
							<span class="fl-button-text">View the full aircraft inventory</span>
					</a>
</div>
<?php endif; ?>
</div>
</div>